Олимпиады из перечня РСОШ (информатика) и предположительные даты проведения для учеников 10-11 класса на 2021/2022 учебные годы

<h1>ТОП</h1>
<p>Всероссийская олимпиада школьников (дата проведения неизвестна)</p>

<h1>I Уровень</h1>
<ul>
    <li>
        Всероссийская олимпиада школьников "Высшая проба" (информатика)
        <ol>
            <li>
                Регистрация: 20 сентября - 28 октября 2021 года
            </li>
            <li>
                Первый (отборочный) заочный этап: 3 ноября - 18 ноября 2021 года
            </li>
            <li>
                Второй (заключительный) этап: 28 января - 6 февраля 2022 года
            </li>
        </ol>
    </li>
    <li>
        Всесибириская открытая олимпиада школьников
        <ol>
            <li>
                Регистрация с 1 октября
            </li>
            <li>
                Основной отборочный этап: 17 октября (9:00 MSK)
            </li>
            <li>
                Дополнительный отборочный этап: 25 декабря - 15 января 
            </li>
            <li>
                Заключительный этап: 13 февраля - 13 марта
            </li>
        </ol>
    </li>
    <li>
        Московская олимпиада школьников (информатика)
        <ol>
            <li>
                Отборочный этап (ориентировочно): 13 января - 9 марта
            </li>
        </ol>
    </li>
    <li>
        Олимпиада НТИ 
        <ol>
            <li>
                Интеллектуальные робототехнические системы
            </li>
        </ol>
        Математика:
        <ol>
            <li>
                Первая попытка: 1 октября - 3 октября 
            </li>
            <li>
                Вторая попытка: 11 октября - 13 октября
            </li>
            <li>
                Третья попытка: 21 октября - 24 октября
            </li>
        </ol>
        Информатика:
        <ol>
            <li>
                Первая попытка: 5 октября - 7 октября 
            </li>
            <li>
                Вторая попытка: 15 октября - 17 октября
            </li>
            <li>
                Третья попытка: 25 октября - 27 октября
            </li>
        </ol>
        Физика:
        <ol>
            <li>
                Первая попытка: 3 октября - 5 октября 
            </li>
            <li>
                Вторая попытка: 13 октября - 15 октября
            </li>
            <li>
                Третья попытка: 23 октября - 25 октября
            </li>
        </ol>
    </li>
    <li>
        Олимпиада по комплексу предметов «Культура и искусство» (прикладная информатика)
        <ol>
            <li>
                Регистрация: ???
            </li>
            <li>
                Отборочный этап: 1 октября - 31 января 
            </li>
            <li>
                Заключительный этап: 20 марта - 31 марта
            </li>
        </ol>
    </li>
    <li>
        Олимпиада школьников «Ломоносов»
        <ol>
            <li>
                Регистрация: ориентировочно в октябре-ноябре
            </li>
            <li>
                Отборочный этап: ориентировочно в октябре-ноябре
            </li>
            <li>
                Заключительный этап: ориентировочно 9 марта
            </li>
        </ol>
    </li>
    <li>
        Олимпиада школьников по информатике и программированию (https://neerc.ifmo.ru/school/ioip/index.html)
        <ol>
            <li>
                Отборочный этап 1: ориентировочно 30 января
            </li>
            <li>
                Отборочный этап 2: ориентировочно 27 февраля
            </li>
            <li>
                Заключительный этап: ориентировочно 28 марта
            </li>
        </ol>
    </li>
    <li>
        Олимпиада школьников по программированию «ТехноКубок»
         <ol>
            <li>
                Первый тур: 25 октября (ориентировочно)
            </li>
            <li>
                Второй тур: 29 ноября (ориентировочно)
            </li>
            <li>
                Третий тур: 20 декабря (ориентировочно)
            </li>
        </ol>
    </li>
    <li>
        Олимпиада школьников Санкт-Петербургского государственного университета (информатика)
         <ol>
            <li>
                Отборочный этап: 20 октября - 12 января
            </li>
            <li>
                Заключительный этап: 1 февраля - 13 марта
            </li>
        </ol>
    </li>
    <li>
        Открытая олимпиада школьников (https://www.olympiads.ru/zaoch/)
        <ol>
            <li>
                Первый отборочный тур: 16 ноября (ориентировочно)
            </li>
            <li>
                Второй отборочный тур: 20 декабря (ориентировочно)
            </li>
        </ol>
    </li>
    <li>
        Открытая олимпиада школьников по программированию (ИТМО)
        <ol>
            <li>
                Первый отборочный тур: 20 ноября (ориентировочно)
            </li>
            <li>
                Второй отборочный тур: 19 января (ориентировочно)
            </li>
        </ol>
    </li>
</ul>
<h1>II Уровень</h1>
<ul>
    <li>
        Всероссийская олимпиада школьников "Высшая проба" (электроника и вычислительная техника)
        <ol>
            <li>
                Регистрация: 20 сентября - 28 октября 2021 года
            </li>
            <li>
                Первый (отборочный) заочный этап: 3 ноября - 18 ноября 2021 года
            </li>
            <li>
                Второй (заключительный) этап: 28 января - 6 февраля 2022 года
            </li>
        </ol>
    </li>
    <li>
        Олимпиада НТИ
        <ol>
            <li>
                Большие данные и машинное обучение
            </li>
            <li>
                Беспилотные авиационные системы
            </li>
            <li>
                Технологии беспроводной связи
            </li>
            <li>
                Автоматизация бизнес-процессов
            </li>
            <li>
                Водные робототехнические системы
            </li>
            <li>
                Нейротехнологии и когнитивные науки
            </li>
            <li>
                Передовые производственные технологии
            </li>
            <li>
                Наносистемы и наноинженерия
            </li>
        </ol>
        Математика:
        <ol>
            <li>
                Первая попытка: 1 октября - 3 октября 
            </li>
            <li>
                Вторая попытка: 11 октября - 13 октября
            </li>
            <li>
                Третья попытка: 21 октября - 24 октября
            </li>
        </ol>
        Информатика:
        <ol>
            <li>
                Первая попытка: 5 октября - 7 октября 
            </li>
            <li>
                Вторая попытка: 15 октября - 17 октября
            </li>
            <li>
                Третья попытка: 25 октября - 27 октября
            </li>
        </ol>
        Физика:
        <ol>
            <li>
                Первая попытка: 3 октября - 5 октября 
            </li>
            <li>
                Вторая попытка: 13 октября - 15 октября
            </li>
            <li>
                Третья попытка: 23 октября - 25 октября
            </li>
        </ol>
    </li>
    <li>
        Олимпиада Университета Иннополис «Innopolis Open» (информатика)
        <ol>
            <li>
                Регистрация: 16 сентября - 1 октября
            </li>
            <li>
                Первый отборочный тур: 21 ноября 10:00-15:00 (MSK)
            </li>
            <li>
                Второй отборочный тур: 12 декабря 15:00-20:00 (MSK)
            </li>
            <li>
                Заключительный этап: 19-20 февраля 
            </li>
        </ol>
    </li>
    <li>
        Олимпиада Университета Иннополис «Innopolis Open» (информационная безопасность)
        <ol>
            <li>
                Регистрация: 16 сентября - 1 октября
            </li>
            <li>
                Первый тур: 29 октября
            </li>
        </ol>
    </li>
    <li>
        Олимпиада школьников «Шаг в будущее»
        <ol>
            <li>
                Программирование
                <ol>
                    <li>
                        1 Волна: 23-26 октября (ориентировочно)
                    </li>
                    <li>
                        2 Волна: 20-23 ноября (ориентировочно)
                    </li>
                    <li>
                        3 Волна: 18-21 декабря (ориентировочно)
                    </li>
                </ol>
            </li>
            <li>
                Инженерное дело
                <ol>
                    <li>
                        1 Волна: 23-26 октября (ориентировочно)
                    </li>
                    <li>
                        2 Волна: 20-23 ноября (ориентировочно)
                    </li>
                    <li>
                        3 Волна: 18-21 декабря (ориентировочно)
                    </li>
                </ol>
            </li>
        </ol>
    </li>
    <li>
        Открытая олимпиада школьников по программированию «Когнитивные технологии»
        <ol>
            <li>
                Регистрация: 15 октября - 1 декабря (ориентировочно)
            </li>
            <li>
                Отборочный этап: 1 декабря - 13 декабря (ориентировочно)
            </li>
        </ol>
    </li>
</ul>
<h1>III Уровень</h1>
<ul>
    <li>
        Вузовско-академическая олимпиада по программированию на Урале
        <ol>
            <li>
                Отборочный этап: 27 февраля - 3 марта (ориентировочно)
            </li>
            <li>
                Заключительный этап: 3 апреля (ориентировочно)
            </li>
        </ol>
    </li>
    <li>
        Межрегиональная олимпиада школьников «САММАТ» (09.03.00 информатика и вычислительная техника)
    </li>
    <li>
        Многопрофильная инженерная олимпиада «Звезда» (информатика и вычислительная техника)
    </li>
    <li>
        Московская олимпиада школьников (робототехника, предпрофессиональная)
    </li>
    <li>
        Олимпиада НТИ
        <ol>
            <li>
                Умный город
            </li>
            <li>
                Программная инженерия финансовых технологий
            </li>
            <li>
                Аэрокосмические системы
            </li>
            <li>
                Интеллектуальные энергетические системы
            </li>
            <li>
                Информационная безопасность
            </li>
            <li>
                Анализ космических снимков и геопространственных данных
            </li>
            <li>
                Спутниковые системы
            </li>
            <li>
                Автономные транспортные системы
            </li>
            <li>
                Летающая робототехника
            </li>
        </ol>
    </li>
    <li>
        Олимпиада школьников «Гранит науки»
        <ol>
            <li>
                Отборочный этап: 30 ноября (ориентировочно)
            </li>
        </ol>
    </li>
    <li>
        Олимпиада школьников «Ломоносов» (робототехника)
        <ol>
            <li>
                Регистрация: ориентировочно в октябре-ноябре
            </li>
            <li>
                Отборочный этап: ориентировочно в октябре-ноябре
            </li>
            <li>
                Заключительный этап: ориентировочно 9 марта
            </li>
        </ol>
    </li>
    <li>
        Олимпиада школьников «Надежда энергетики»
        <ol>
            <li>
                Информатика
                <ol>
                    <li>
                        1 поток: ноябрь
                    </li>
                    <li>
                        2 поток: декабрь
                    </li>
                </ol>
            </li>
            <li>
                Комплекс предметов (физика, информатика, математика)
                <ol>
                    <li>
                        1 поток: ноябрь
                    </li>
                    <li>
                        2 поток: декабрь
                    </li>
                </ol>
            </li>
        </ol>
    </li>
    <li>
        Олимпиада школьников «Шаг в будущее»
        <ol>
            <li>
                Компьютерное моделирование и графика (ориентировочно 29 января - 2 февраля)
            </li>
        </ol>
    </li>
    <li>
        Олимпиада школьников Санкт-Петербургского государственного университета (инженерные системы)
         <ol>
            <li>
                Отборочный этап: 20 октября - 12 января
            </li>
            <li>
                Заключительный этап: 1 февраля - 13 марта
            </li>
        </ol>
    </li>
    <li>
        Отраслевая олимпиада школьников «Газпром»
         <ol>
            <li>
                Подготовительный этап: 10 октября - 31 октября
            </li>
            <li>
                Отборочный этап: 1 ноября - 12 января
            </li>
            <li>
                Заключительный этап: февраль-март
            </li>
        </ol>
    </li>
    <li>
        Университетская олимпиада школьников «Бельчонок»
         <ol>
            <li>
                Отборочный этап: 1 октября - 13 января (ориентировочно)
            </li>
            <li>
                Заключительный этап: 6-7 марта или 13-14 марта (ориентировочно)
            </li>
        </ol>
    </li>
</ul>

<h1> Можно ли еще как-то получить преимущество при поступлении в вуз? </h1>
<p> Да, еще можно поучаствовать в конкурсах и получить до 10 баллов ЕГЭ за интеллектуальные достижения. Вот перечень олимпиад и конкурсов на 2020-2021 год: <a href="https://regulation.gov.ru/projects#npa=110788">https://regulation.gov.ru/projects#npa=110788 </a></p>
